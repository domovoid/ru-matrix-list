## Русскоязычные комнаты всея matrix

### Без конкретной темы, главные комнаты
- Русскоязычная комната: https://matrix.to/#/!mHIzfmDaswOshUSeBd:matrix.org?via=matrix.org&via=tchncs.de&via=kde.org
- Вежливая русскоязычная комната: https://matrix.to/#/#ru.talks:matrix.org
- ru.matrix: https://matrix.to/#/#ru.matrix:matrix.org  Админ - Magnolia, стиль модерации предвзят и политизирован


### Интересные пространства
- Русскоязычное сообщество Matrix: https://matrix.to/#/#russian:ru-matrix.org
- Русскоязычные комнаты: https://matrix.to/#/#ru.collection:matrix.org Админ - Magnolia, стиль модерации предвзят и политизирован
- tharg.ru : https://matrix.to/#/#general:tharg.ru Направление комнаты - как общее, "жизненное", так и связанное с СПО, опенсурсом, борьбой за информационную свободу и криптомиром. Регистрации на сервере открыты, правило одно - соблюдать законы о киберпространстве в Германии (без чернухи всякой и всё).


### Линуксовые комнаты
- GNU/Linux in Russian: https://matrix.to/#/#linux.ru:matrix.org
- GNU/Linux in Russian V2: https://matrix.to/#/#linux_v2.ru:matrix.org
- NixOS-RU: https://matrix.to/#/!ZEUixuGkHFYKByHHYn:matrix.org?via=matrix.org&via=nope.chat&via=sibnsk.net
- openwrt на русском: https://matrix.to/#/#openwrt-rus:matrix.org
- Главная/General tharg.ru : https://matrix.to/#/#main:tharg.ru
- GNOME Ru: https://matrix.to/#/#gnome-ru:matrix.org
- nixdev: https://matrix.to/#/#ru.nixdev:matrix.org
- KDE Россия: https://matrix.to/#/#kde_ru:kde.org
- UfaLUG: https://matrix.to/#/#ufalug:tchncs.de
- Lugnsk: https://matrix.to/#/#lugnsk:sibnsk.net
- Russian Fedora: https://matrix.to/#/#russianfedora:matrix.org
- Linux gaming Ru: https://matrix.to/#/#linux-ru-gaming:matrix.org
- Flatpak Ru: https://matrix.to/#/#flatpak-ru:matrix.org
- Rosa Linux: https://matrix.to/#/!PquENuVrJGHTQabuhl:matrix.org?via=t2bot.io&via=matrix.org&via=internet-portal.cz либо [тут](https://matrix.to/#/#rosalinux-ru:matrix.org)
- RULinux NEWS: https://matrix.to/#/#rulinuxnews:matrix.org


### Поддержка и обсуждение matrix
- Matrix на русском: https://matrix.to/#/#matrix-ru:ru-matrix.org
- matrix Русская техподдержка: https://matrix.to/#/#matrix-support-ru:matrix.org
- Перевод Element на русский: https://matrix.to/#/#element-translation-ru:matrix.org


### Технические чаты
- Канал русскоязычного сообщества OpenStreetMap: https://matrix.to/#/#osm-ru:matrix.org
- Organic Maps по-русски: https://matrix.to/#/#organicmapsru:matrix.org
- AOSP-RU: https://matrix.to/#/#aosp-ru:matrix.org
- RADIUM: https://matrix.to/#/#radium:tchncs.de
- Программирование и техника: https://matrix.to/#/#tech:ru-matrix.org
- Умный дом и автоматизация устройств: https://matrix.to/#/#hassio:gazizova.net
- Sailfish in Russian: https://matrix.to/#/#ru.sailfish:matrix.org
- Causa Arcana: https://matrix.to/#/#causa-arcana:matrix.org
- RaspberryPi-RU: https://matrix.to/#/!eIgrXZvakqCsYryUgu:matrix.org?via=matrix.org
- ru - Протоколы (IRC, TG, Tox, XMPP): https://matrix.to/#/#ru.protocols:matrix.org
- IPv6: https://matrix.to/#/#ipv6-ru:matrix.org


### Книги, фильмы, сериалы, искусство и другое
- Аниме: https://matrix.to/#/#anime-ru:matrix.org
- ru.books: https://matrix.to/#/#ru.books:matrix.org (Не слишком активны) Админ - Magnolia, стиль модерации предвзят и политизирован
- Анекдоты: https://matrix.to/#/#anekdoty:matrix.org
- Music of Space: https://matrix.to/#/#music_of_space:matrix.org
- Аниме: https://matrix.to/#/#Комфортно_анимешно_круто:matrix.org
- Ламповая музыка: https://matrix.to/#/#lampmusic:matrix.org
- Игры: https://matrix.to/#/#ru.lng.games:matrix.org
- Игровой зал: https://matrix.to/#/#ru-gaming:matrix.org


### Наука и научпоп
- Астрономия & Космос: https://matrix.to/#/#astro1:matrix.org
- Kaleidoscope: https://matrix.to/#/#kaleidoscope:matrix.org


### Иностранные языки и культура
- Япония: https://matrix.to/#/#ru.japan:matrix.org

### Блоги/Каналы
- Zero_blog: https://matrix.to/#/#zero_blog:matrix.org

### Другие
- [ru.btc](https://matrix.to/#/#ru.btc:matrix.org)
- Laterna Magica: https://matrix.to/#/#ru.magic-lantern:matrix.org
- [XMR.RU](https://matrix.to/#/#xmr.ru:matrix.org)
- Привет всем!  (Зайди плиз в чат, тут будет круто): https://matrix.to/#/!yrknHFwAWERhhTJeci:matrix.org?via=matrix.org&via=envs.net
- [Глагне. InnerEq.org](https://matrix.to/#/#degeneral:sunbutt.faith)
- Cyclonegpt-ru (эксперимент [darkcoder15](@darkcoder15:m.darkcoder15.tk)): https://matrix.to/#/#cyclonegpt-ru:m.darkcoder15.tk

### Matrix мосты
- Astra Linux: https://matrix.to/#/#astra_linux:matrix.org
- openSUSE | RU: https://matrix.to/#/#opensuse-ru:matrix.org
- rulinuxru: https://matrix.to/#/#rulinux:matrix.org
- Бессонница 2: https://matrix.to/#/#bessonnica2:matrix.org
- OpenE2K: https://matrix.to/#/!ukEcZpCvuPXuuvPKWl:kde.org?via=kde.org&via=t2bot.io&via=matrix.org
- ReactOS Russian / Русский: https://matrix.to/#/#lang-ru:reactos.org
- ReactOS Telegram bridge (ru-RU): https://matrix.to/#/#telegram-ru:reactos.org

### Молодые и начинающие чаты(Меньше 5-10 человек)
- Чат: https://matrix.to/#/!MtFzZTmoJmuExhUEHx:matrix.org?via=matrix.org
- Сообщество Юных русских флейтистов: https://matrix.to/#/#Флейта_для_Всех!___Флейта_самый_лучший_инструмент!:matrix.org
- Склад нужных мне ссылок: https://matrix.to/#/#Сылочки_путник_friend12345:matrix.org

[Полумертвые и мертвые комнаты](https://codeberg.org/domovoid/ru-matrix-list/src/branch/master/dead_rooms.md)

Предлагайте комнаты, которые нужно добавить в [Matrix на русском](https://matrix.to/#/#matrix-ru:ru-matrix.org) , в этой же комнате мы обсуждаем и другие вопросы связанные с улучшение списка и пространств в ru-matrix
Убедительная просьба писать в личку только в случае игнорирования сообщений в вышеупомянутой комнате на протяжении недели!

Можете поддержать автора списка, так он будет актуальнее, удобнее и лучше:
- Ethereum: 0xA48A68B1Ca9459c044353C8bE28b78DB64F2d722
- Monero: 82wYsxMj5hVXvFNqG69JHXiWMx5no8aABFDqkRwmbQi66WFr335vsvd2mb9qJdVUP9bU418CpSpqajZiSdD3yf3RLYAoncU
- USDT: 0xA48A68B1Ca9459c044353C8bE28b78DB64F2d722
- Карту банка спрашивать в личке


Дорогие пользователи, модераторы и иные обитатели matrix, распространяйте данный список, закрепляйте его в шапке каждой комнаты, чтобы в ru-matrix был длинный список активных и интересных комнат, которые мог бы найти каждый
[Исходный код списка](https://codeberg.org/domovoid/ru-matrix-list)
Последние изменения:
		- Помечены комнаты Magnolia(возможно не все)
		- Добавлен tharg.ru , Zero_blog
